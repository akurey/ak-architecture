/**
 * Filename: logger-options.ts
 * Author: rnunez@akurey.com
 * Date: 03/08/2019
 * Description: Options to setup wiston logger
 */

import winston from 'winston';

// Configure dotenv for custom process.env variables
const env = process.env.NODE_ENV || 'local';
const isLocal = env === 'local';

// const filePath = (isLocal) ? './logs/' : './logs/';
const filePath = './logs/';

export const loggerColors = {
  error: 'red',
  warn: 'white',
  info: 'white',
  verbose: 'green',
  debug: 'blue',
  event: 'yellow',
};

export const loggerOptions = {
  colorize: true,
  meta: true,
  levels: { 
    error: 0, 
    warn: 1, 
    info: 2, 
    verbose: 3,
    debug: 4,
    event: 5, 
  },
  transports: [
    new winston.transports.Console({
      format: winston.format.combine(
        winston.format.colorize(),
        winston.format.json(),
        winston.format.prettyPrint(),
        winston.format.printf(info => `[${info.level}]: ${info.message}`),
      ),
      level: 'verbose',
    }),
    new winston.transports.File({
      filename: `${filePath}error.log`,
      level: 'error',
    }),
    new winston.transports.File({
      filename: `${filePath}info.log`,
      level: 'info',
    }),
    new winston.transports.File({
      filename: `${filePath}verbose.log`,
      level: 'verbose',
    }),
    new winston.transports.File({
      filename: `${filePath}events.log`,
      level: 'event',
    }),
  ],
};
