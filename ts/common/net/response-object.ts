/**
 * Filename: response-object.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: To encapsulate the response object to be send in response to the frontend
 */

import { Request } from 'express';

export class ResponseObject {
  data: any;
  elapseTime: number;
  clientTimeStamp: Date;
  serverTimeStamp: Date;

  constructor(pData: any, pReq: Request) {
    this.data = pData;
    this.clientTimeStamp = pReq.body.clientTimeStamp;
    this.serverTimeStamp = new Date();
    this.elapseTime = (new Date()).getTime() - pReq.body.clientTimeStamp.getTime();
  }
}
