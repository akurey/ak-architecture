/**
 * Filename: log.util.ts
 * Author: lcruz@akurey.com
 * Date: 12/12/2018
 * Description: Log Utility which exports log config and winston logger
 * Modify by rnunez in order to standarize the log object
 */

import crypto from 'crypto';
import winston from 'winston';
import { Credential } from '../security/credential';
import { loggerColors, loggerOptions } from './logger-options';
import { Log } from './log';

// Configure dotenv for custom process.env variables
const env = process.env.NODE_ENV || 'local';

export class Logger {
  public static getInstance() {
    if (!Logger.instance) {
      Logger.instance = new Logger();
    }
    return Logger.instance;
  }

  private static instance: Logger;
  public logger: winston.Logger;

  /**
   * Initialize the queue and wiston logger
   */
  private constructor() {
    this.logger = winston.createLogger(loggerOptions);
    winston.addColors(loggerColors);
  }

  /**
   * Log a verbose msg
   * @param pMsg 
   */
  public verbose(pMsg: string) {
    this.logger.log('verbose', pMsg);
  }

  /**
   * Log error messages
   * @param pMsg 
   */
  public error(pMsg: string) {
    this.logger.log('error', pMsg);
  }

  /** 
  * Log info messages
  * @param pMsg 
  */
  public info(pMsg: string) {
    this.logger.log('info', pMsg);
  }

  /** 
  * Log info messages
  * @param pMsg 
  */
  public warn(pMsg: string) {
    this.logger.warn('warn', pMsg);
  }

  /**
   * General log action with multiple variations
   * @param pType 
   * @param pDescription 
   * @param pCredential 
   * @param pLogTime 
   */
  public log(pType: string, pDescription: string, pCredential: Credential, pLogTime: string) : void;
  public log(pType: string, pDescription: string, pCredential: Credential, pLogTime: string, pDetails: string) : void;
  public log(pType: string, pDescription: string, pCredential: Credential, pLogTime: string, pDetails: string, pResponse?: string) : void;
  public log(pType: string, pDescription: string, pCredential: Credential, pLogTime: string, pDetails?: string, pResponse?: string, pTarget1?: string, pTarget2?: string, pRefValue1?: string, pRefValue2?: string) : void;
  public log(pType: string, pDescription: string, pCredential: Credential, pLogTime: string, pDetails?: string, pResponse?: string, pTarget1?: string, pTarget2?: string, pRefValue1?: string, pRefValue2?: string) : void {
    const log = new Log(pType, pDescription);
    log.computer = pCredential && pCredential.computer ? pCredential.computer : '';
    log.user = pCredential && pCredential.user ? pCredential.user : '';
    log.ip = pCredential && pCredential.ip ? pCredential.ip : '';
    log.logtime = new Date(pLogTime) || log.logtime;
    log.details = pDetails || '';
    log.response = pResponse || '';
    log.target1 = pTarget1 || '';
    log.target2 = pTarget2 || '';
    log.refvalue1 = pRefValue1 || '';
    log.refvalue2 = pRefValue2 || '';
    log.responsetime = log.posttime.getTime() - log.logtime.getTime();
    
    const hmac = crypto.createHmac('sha256', '2782l893j83kdkld93jdp2kds9mkkf9820dkdj810wsk');

    const checkedValue = `1111${log.type}${log.refvalue1}893jkf9820dk${log.refvalue2}${log.target1}2782ldj810wsk${log.target2}---${log.logtime}--rnunez_inthefloor-${log.posttime}${log.user}83kdkld93jdp2kds9mk-drSpock${log.computer}${log.ip}`
    hmac.update(checkedValue);
    log.checksum = hmac.digest('hex');

    this.logger.log('event', JSON.stringify(log));
  }

}