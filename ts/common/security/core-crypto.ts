/**
 * Filename: core-crypto.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: Encrypt and decrypt functions based on RSA of auto generated keys
 */

import NodeRSA from 'node-rsa';
const ENCRYPT_ENCODING = 'base64';
const DECRYPT_ENCODING = 'utf8';
const WAIT_TIME_KEY_CREATION = 0;


export class CoreCrypto {
  private publicKey: string;
  private privateKey: string;

  /**
   * Creates a new instance capable of encrypt/decrypt
   * @param pGenerateNewKeys , if true, new keys are generated, by the contrary the keys must be provide properly 
   */
  constructor(pGenerateNewKeys: boolean) {
    this.privateKey = '';
    this.publicKey = '';

    if (pGenerateNewKeys) {
      this.createKeyPair()
      .then((keys:any) => {
        this.publicKey = keys.publicKey;
        this.privateKey = keys.privateKey;
      })
      .catch(() => {
        this.publicKey = '';
        this.privateKey = '';
      });
    }
  }

  public getPublicKey() {
    return this.publicKey;
  }

  public getPrivateKey() {
    return this.privateKey;
  }

  /**
   * Asymetric encryption based on given key
   * @param pData text to be encrypted
   * @param pKey RSA private/public key to be use for encryption 
   */
  public static encrypt(pData: string, pKey: string) : string {
    const key = new NodeRSA(pKey);
    return key.encrypt(pData, ENCRYPT_ENCODING);
  }

  /**
   * Asymetric decryption based on given key
   * @param pData text to be decrypted
   * @param pKey RSA private key to be use for decryption 
   */
  public static decrypt(pData: string, pPrivateKey: string) : string {
    const key = new NodeRSA(pPrivateKey);
    return key.decrypt(pData, DECRYPT_ENCODING);
  }

  /**
   * Generate one asymetric key pair 
   */
  private createKeyPair() {
    return new Promise<any>((resolve, reject) => {
        setTimeout(()=>{ // this timeout is call in order to handle the sync process of RSA
          try {
            const key = new NodeRSA({ b: 2048});

            const privateKey = key.exportKey(),
                  publicKey = key.exportKey('public');
            
            resolve({ privateKey, publicKey });
          } catch {
            reject({privateKey: '', publicKey: ''});
          }  
        }, WAIT_TIME_KEY_CREATION);
    });
  };

}

// for (let countCrytos = 0; countCrytos < 10; countCrytos += 1) {
//   const x = new CoreCrypto(true);
//   console.log(`contador for ${countCrytos}`);
// }