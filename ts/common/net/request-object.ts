/**
 * Filename: request-object.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: To encapsulate the request object sent in all rest messages
 */

import { CoreCrypto } from '../security/core-crypto';
import { Credential } from '../security/credential';

export class RequestObject {
  data: any;
  computer: string;
  ip: string;
  clientTimeStamp: Date;
  serverTimeStamp: Date;
  private credential: Credential;
  private secure: any;

  constructor(pCredential: Credential, pComputer: string, pIp: string, pData: any) {
    this.data = pData;
    this.computer = pComputer ? pComputer : '';
    this.ip = pIp ? pIp : '';
    this.clientTimeStamp = new Date();
    this.serverTimeStamp = new Date();
    this.credential = pCredential;
    this.secure = {};
  }

  public setSecure(pValue: string) {
    this.secure = CoreCrypto.encrypt(pValue, this.credential.publicKey);
  }
}
