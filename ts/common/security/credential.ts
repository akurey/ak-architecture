/**
 * Filename: credential.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: Object to store user information regarding its identity, tokens and cryptograpy keys 
 */

import { CredentialType } from './credential-type';

export class Credential {
  user: string;
  publicKey: string;
  computer: string;
  ip: string;
  groupList: string[];

  private tokens: any = {};

  constructor(pUser: string, pPublicKey: string) {
    this.user = pUser;
    this.publicKey = pPublicKey;
    this.computer = '';
    this.ip = '';
    this.groupList = [];
  }

  public addToken(pCredentialType: CredentialType, pValue: string) {
    this.tokens[pCredentialType] = pValue;
  }

  public getToken(pCredentialType: CredentialType) : string {
    return this.tokens[pCredentialType] || '';
  }

  public setDeviceName(pName: string) {
    this.computer = pName;
  }

  public setDeviceIp(pIp: string) {
    this.ip = pIp;
  }

}
