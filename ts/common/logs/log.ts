import { string } from "joi";

/**
 * Filename: log.util.ts
 * Author: rnunez
 * Date: 03/05/2019
 * Description: Generic log object to be stored
 */


export class Log {
  type: string;
  description: string;
  computer: string;
  ip: string;
  user: string;
  posttime: Date;
  responsetime: number;
  logtime: Date;
  details: string;
  response: string;
  target1: string;
  target2: string;
  refvalue1: string;
  refvalue2: string;
  checksum: string;

  constructor(pType: string, pDescription: string) {
    this.type = pType;
    this.description = pDescription;
    this.computer = '';
    this.ip = '';
    this.user = '';
    this.posttime = new Date();
    this.logtime = new Date();
    this.responsetime = 0;
    this.details = '';
    this.response = '';
    this.target1 = '';
    this.target2 = '';
    this.refvalue1 = '';
    this.refvalue2 = '';
    this.checksum = '';
  }
}