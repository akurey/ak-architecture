"use strict";
/**
 * Filename: core-crypto.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: Encrypt and decrypt functions based on RSA of auto generated keys
 */
exports.__esModule = true;
var crypto2_1 = require("crypto2");
var ENCODING = 'utf-8';
var CoreCrypto = /** @class */ (function () {
    /**
     * Creates a new instance capable of encrypt/decrypt
     * @param pGenerateNewKeys , if true, new keys are generated, by the contrary the keys must be provide properly
     */
    function CoreCrypto(pGenerateNewKeys) {
        var _this = this;
        this.privateKey = '';
        this.publicKey = '';
        if (pGenerateNewKeys) {
            crypto2_1.createKeyPair()
                .then(function (keys) {
                _this.publicKey = keys.publicKey;
                _this.privateKey = keys.privateKey;
                console.log('after then');
            })["catch"](function () {
                _this.publicKey = '';
                _this.privateKey = '';
            });
        }
        console.log('fin del constructor');
    }
    CoreCrypto.prototype.getPublicKey = function () {
        return this.publicKey;
    };
    CoreCrypto.prototype.getPrivateKey = function () {
        return this.privateKey;
    };
    /**
     * Asymetric encryption based on given key
     * @param pData text to be encrypted
     * @param pKey RSA private/public key to be use for encryption
     */
    CoreCrypto.prototype.encrypt = function (pData, pKey) {
        return new Promise(function (resolve) {
            crypto2_1.encrypt.rsa(pData, pKey)
                .then(function (result) {
                resolve(result);
            })["catch"](function (err) {
                resolve('NA_crypto' + err);
            });
        });
    };
    /**
     * Asymetric decryption based on given key
     * @param pData text to be decrypted
     * @param pKey RSA private/public key to be use for decryption
     */
    CoreCrypto.prototype.decrypt = function (pData, pKey) {
        return new Promise(function (resolve) {
            crypto2_1.decrypt.rsa(pData, pKey)
                .then(function (result) {
                resolve(result);
            })["catch"](function (err) {
                resolve(pData);
            });
        });
    };
    return CoreCrypto;
}());
exports.CoreCrypto = CoreCrypto;
var nuevo = new CoreCrypto(false);
console.log("ya termine");
