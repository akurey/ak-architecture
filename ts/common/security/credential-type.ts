/**
 * Filename: credential-type.ts
 * Author: rnunez@akurey.com
 * Date: 12/20/2018
 * Description: Enums to manage encryption and credentials
 */

export enum CredentialType {
    CUSTOM = 1,
    AWS,
    AWS_COGNITO_AUTHO_TOKEN,
    AWS_COGNITO_ID_TOKEN,
    POWER_BI,
}

export enum KeyType {
    PRIVATE = 1,
    PUBLIC,
}
