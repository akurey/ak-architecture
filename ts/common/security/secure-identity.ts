
/**
 * Filename: secure-identity.ts
 * Author: rnunez@akurey.com
 * Date: 01/16/2019
 * Description: Class to store the user credential and it's cryptographic resources
 */

import { Credential } from "./credential";
import { CoreCrypto } from "./core-crypto";


/**
 * Object to securely be store, it keeps the association among the credential and it's private keys
 */
export class SecureIdentity {
  private credential: Credential;
  private crypto: CoreCrypto;

  constructor(pCredential: Credential, pCrypto: CoreCrypto) {
    this.credential = pCredential;
    this.crypto = pCrypto;
  }

  public getCredential() : Credential {
    return this.credential;
  }

  public getCrypto() : CoreCrypto {
    return this.crypto;
  }
}
